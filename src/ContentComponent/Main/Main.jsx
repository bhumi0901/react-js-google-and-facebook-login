import React, { Component } from "react";
import logo from "../../Images/ic_logo_C.png";
import "./Main.scss";
import FacebookLogin from "react-facebook-login";
import { GoogleLogin } from "react-google-login";
import Home from "../Home";

class Main extends Component {
  state = {
    isLogin: false
  };
  responseFacebook = response => {
    if (response.accessToken) {
      if (response.name) {
        localStorage.setItem("name", response.name);
      }
      if (response.email) {
        localStorage.setItem("email", response.email);
      }
      if (response.picture) {
        localStorage.setItem("profile", response.picture.data.url);
      }
      this.setState({
        isLogin: true
      });
    }
  };
  componentDidMount() {
    let name = localStorage.getItem("name");
    if (name) {
      this.setState({
        isLogin: true
      });
    } else {
      this.setState({
        isLogin: false
      });
    }
  }
  handelLogout = () => {
    localStorage.removeItem("name");
    localStorage.removeItem("email");
    localStorage.removeItem("profile");
    this.setState({
      isLogin: false
    });
  };
  responseGoogle = response => {
    if (response.tokenObj) {
      if (response.profileObj) {
        let data = response.profileObj;

        if (data.name) {
          localStorage.setItem("name", data.name);
        }
        if (data.email) {
          localStorage.setItem("email", data.email);
        }
        if (data.imageUrl) {
          localStorage.setItem("profile", data.imageUrl);
        }
        this.setState({
          isLogin: true
        });
      }
    }
  };
  render() {
    let { isLogin } = this.state;
    return (
      <>
        {isLogin ? (
          <Home handelLogout={this.handelLogout} />
        ) : (
            <div className="main-container">
              <div>
                <img src={logo} className="logo-image" alt="Logout" />
              </div>
              <div className="react-text">React JS</div>
              <div className="btn-container">
                <div className="facebook-button mr-30" onClick={this.logoutFacebook}>
                  <div className="logo">f</div>
                  <div className="btn">
                    <FacebookLogin
                      autoLoad={false}
                      appId="845409182619981"
                      fields="name,email,picture"
                      callback={this.responseFacebook}
                    />
                  </div>
                </div>
                <div className="google-button">
                  <div className="logo">G</div>
                  <div className="btn">
                    <GoogleLogin
                      clientId="1090383787573-e0jlh066emnrbcsv09uusuoaa6k3smoj.apps.googleusercontent.com"
                      buttonText="Login with Google"
                      onSuccess={this.responseGoogle}
                      onFailure={this.responseGoogle}
                      cookiePolicy={"single_host_origin"}
                      className="google-button"
                      disabled={false}
                      autoLoad={false}
                    />
                  </div>
                </div>
              </div>
            </div>
          )
        }
      </>
    );
  }
}
export default Main;
