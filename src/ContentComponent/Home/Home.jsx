import React, { Component } from "react";
import "./Home.scss";
import logoutImage from "../../Images/logout_image.png";

export default class Home extends Component {
  render() {
    return (
      <div className="home">
        <div className="profile-image">
          <img src={localStorage.getItem("profile")} alt="profile" />
        </div>
        <div className="profile-name">{localStorage.getItem("name")}</div>
        <div
          className="logout-btn"
          onClick={() => {
            this.props.handelLogout();
          }}
        >
          <img src={logoutImage} alt="Logout" />
        </div>
      </div>
    );
  }
}
